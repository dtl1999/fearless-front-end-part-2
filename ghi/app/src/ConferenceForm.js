import React, { useState, useEffect } from 'react';

function ConferenceForm(props) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [startDatetime, setStartDatetime] = useState('');
  const [endDatetime, setEndDatetime] = useState('');
  const [location, setLocation] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('http://localhost:8000/api/locations/');

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    };
    fetchData();
  }, []);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleStartDatetimeChange = (event) => {
    setStartDatetime(event.target.value);
  };

  const handleEndDatetimeChange = (event) => {
    setEndDatetime(event.target.value);
  };

  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name,
      start_datetime: startDatetime,
      end_datetime: endDatetime,
      location,
      description,
    };

    console.log(data);

    const url = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStartDatetimeChange}
                placeholder="Start Datetime"
                required
                type="datetime-local"
                name="start_datetime"
                id="start_datetime"
                className="form-control"
              />
              <label htmlFor="start_datetime">Start Datetime</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEndDatetimeChange}
                placeholder="End Datetime"
                required
                type="datetime-local"
                name="end_datetime"
                id="end_datetime"
                className="form-control"
              />
              <label htmlFor="end_datetime">End Datetime</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleUrlChange} placeholder="Url" required type="url" name="url" id="url" className="form-control" value={url} />
                <label htmlFor="url">Url</label>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
      </div>
    </div>
    );
}

export default ConferenceForm
